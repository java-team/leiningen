the debianisation of leiningen
==============================

The Debian packaging for leiningen [0] is stored in git. You can use
debcheckout or clone it directly from the repository:

    $ debcheckout leiningen
    $ git clone git://git.debian.org/pkg-java/leiningen.git

If you are a member of the pkg-java team, you probably want to be able to push
too:

    $ git clone ssh://git.debian.org/git/pkg-java/leiningen.git

This package uses a version control system as described in
http://wiki.debian.org/Java/JavaVcs and the pages linked from there.

Note on versions <= 1.6.1-2
---------------------------

We used gitpkg [1] to build the package up to version 1.6.1-2. In order to
generate the orig tarball and patches you need to enable the corresponding
export hooks of gitpkg and configure gitpkg to build with dpkg-buildpackage:

    $ git config gitpkg.pre-export-hook /usr/share/gitpkg/hooks/pristine-tar-pre-export-hook
    $ git config gitpkg.deb-export-hook /usr/share/gitpkg/hooks/quilt-patches-deb-export-hook
    $ git config gitpkg.exit-hook /usr/share/gitpkg/hooks/dpkg-buildpackage-exit-hook

and you can then build a specific version:

    $ gitpkg debian/$debian_ver

Due to switching to the endorsed git packaging style described in [2] a rename
of the patch-queue branch to patch-queue/old was necessary in order to be
compatible with git-pq's branch naming scheme. If you checked out leiningen
prior to this version you need to run the following commands to update your
local repository.

    $ git remote prune origin
    $ git branch -D patch-queue
    $ git pull --all

[0] https://github.com/technomancy/leiningen
[1] http://packages.debian.org/sid/gitpkg
[2] http://wiki.debian.org/Java/JavaGit
