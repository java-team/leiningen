(defproject dev-deps-only "1.0.0-SNAPSHOT"
  :java-source-path "src"
  :dev-dependencies [[cheshire "2.0.2"]
                     [org.clojure/clojure "1.2.1"]]
  :main dev_deps_only.Junk)